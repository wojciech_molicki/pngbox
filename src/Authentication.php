<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of Authentication
 *
 * @author Wojtek
 */
require_once('./src/Database.php');
require_once('./src/ErrorCodes.php');
require_once('./src/Logging.php');
require_once('./src/Session.php');

require_once('./src/pwd.php');


class TAuthentication {  
//auth class
  
  function __construct() {
    $this->Logging = new TLogging();
    
    $this->MAX_FAIL_LOGINS = 3;
    
    if ($_SESSION['logged_in'] == 1) {
      $this->isAuthorised = 1;
    } else {
      $this->isAuthorised = 0;
    }
    
    global $pepper;
    global $db_pass;
    
    $this->pepper = $pepper;
    $this->db_pass = $db_pass;
    
  }
  function isAuthorized($username, $password) {
    //return whether this username is authorised
    echo "zalogowano = " . $this->isAuthorised;
    return $this->isAuthorised;
  }
  function checkUserPass($username, $password) {
    if (($this->checkIfValidPostData($username) == OPERATION_SUCCESS) && ($this->checkIfValidPostData($password) == OPERATION_SUCCESS)) {
      
      $db = new TDatabase();
      $sqlQuery = "select * from users where username='$username';";
      
      $result = $db->queryDatabase($sqlQuery);
      
      if (empty($result)) { return USER_NOT_IN_DATABASE; }
      else { 
        if ($result[0]['fail_login_count'] < $this->MAX_FAIL_LOGINS) {
          // gets hashed password and salt, checks whether user provided password will produce
          // same hash given salt from db and pepper from pwd.php
          // if ($result[0]['password'] == md5($password)) {
          //
          $h = $result[0]['password'];
          $s = $result[0]['salt'];
          if ($this->validPass($password, $h, $s)) {
              if ($result[0]['fail_login_count'] > 0) {
                //reset fail login count after successful login
                $sqlQuery = "update users set fail_login_count = 0 where username='$username';";
                $db->queryDatabase($sqlQuery);
              }
              session_destroy();
              $Session = new TSession();
              $_SESSION['logged_in'] = 1;
              $_SESSION['username'] = $result[0]['username'];
              $_SESSION['uniq_id'] = $result[0]['id'];
              
              
              return LOGIN_SUCCESS;
            }
            else { 
              $sqlQuery = "update users set fail_login_count = fail_login_count + 1 where username='$username';";
              $db->queryDatabase($sqlQuery);
              return WRONG_PASSWORD;
            }
          }
        else { 
          $this->Logging->log("Account $username locked. (" . FAIL_LOGIN_EXCEEDED . ")");
          return FAIL_LOGIN_EXCEEDED; }
        }
    }
    else { return DATA_NOT_VALID; }
  }
  
  function createUser($username, $password) { 
    echo $password;
    $h = $this->makePasswordHash($password);
    $sqlQuery = "insert into users (username, password, salt) values ('$username', '" . $h[0] . "', '" . $h[1] . "');";
    $db = new TDatabase();
    $result = $db->insertUserIntoDatabase($sqlQuery);
    return $result;
  }
  
  function checkIfValidPostData($data) {
    if ((strlen($data) > 60)) {
      return DATA_NOT_VALID;
    }
    if ((strlen($data) == 0)) {
      return DATA_NOT_VALID;
    }
    $count = 0;
    preg_replace('![^a-z0-9]!imsx', '', $data, -1, $count);
    if ($count != 0) {
      return DATA_NOT_VALID;
    }
    return OPERATION_SUCCESS;
  }
  
  //security
  
  function make_salt() {
  //returns salt which is 60 random letters
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $salt = '';
    for ($i = 0; $i < 20; $i++) {
      $salt .= $chars[mt_rand(0, strlen($chars)-1)];
    }
    return $salt;
  }
  
  function makePasswordHash($password, $salt = '') {
    //returns an array of: hashed password and salt
    $retArr = array();
    if ($salt == '') { $salt = $this->make_salt(); }
    $retArr[] = md5($password . $salt . $this->pepper);
    $retArr[] = $salt;
    return $retArr;
  }
  
  function validPass($password, $hash, $salt) {
    //compares hashes
    $h = $this->makePasswordHash($password, $salt)[0];
    return ($h === $hash);
  }
  
}
