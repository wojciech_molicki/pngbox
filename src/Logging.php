<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of Logging
 *
 * @author Wojtek
 */
class TLogging {
  //put your code here
  function __construct() {
    $this->logfile = './logs/log_' . date('Ymd') . '.txt';
  }
          
  
  function log($message) {
    $file = fopen($this->logfile, 'a+');
    $log_message = date("Y-m-d h:i:s", time()) . " " . $message . "\n";
    
    
    
    fwrite($file, $log_message);
    fclose($file);
  }
}
