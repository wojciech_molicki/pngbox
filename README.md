pngbox
========

####pngBOX Main Repository

Image uploader/browser web application in PHP, using MVC design pattern. 

Basic functionality: Signing up/logging in, adding images with watermarks, adding private images for logged in users.