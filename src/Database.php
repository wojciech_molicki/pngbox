<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of Database
 *
 * @author Wojtek
 */

require_once("./src/Logging.php");

class TDatabase {
  //put your code here
  function __construct() {
    $this->Logging = new TLogging();
    try {
      $this->db = new PDO("mysql:host=localhost;dbname=galeria_zdjec;encoding=utf8", "galeria", "password");
      $this->Logging->log("Connected to database");
    }
    catch(PDOException $e) {
      $this->Logging->log("ERROR: Could not connect to database! " . $e->getMessage());
      die;
    }
  }
  
  function queryDatabase($query) {
   $results = $this->db->prepare($query);
   $results->execute();
      
   $rows = $results->fetchAll();
   return $rows;
  }
  
  function insertUserIntoDatabase($query) { 
    //returns 0 if success, returns 1 for duplicate entry, 2 for another error
    
      $res = $this->db->prepare($query);
      $res->execute();
      
      //mysql error code 1062 for duplicate entry
      if ($res->errorInfo()[1] == 1062) { return USER_EXISTS; }
      elseif ($res->errorInfo()[1] == 0) {return OPERATION_SUCCESS; }
      else { return DATABASE_ERR; }
    
  }
}
