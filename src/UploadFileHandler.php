<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of TUploadFileHandler
 *
 * @author Wojtek
 */
require_once("./src/XMLGenerator.php");
require_once("./src/Authentication.php");

class TUploadFileHandler {
  
  function __construct(&$post, &$files) {
    $this->tmp_folder = "C:/wamp/tmp/";
    $this->images_folder = "./public/images/";
    if (!file_exists($this->images_folder)) { 
      mkdir($this->images_folder, 0777, true ); 
      if (!file_exists($this->images_folder . "watermarks/")) { mkdir($this->images_folder . "watermarks/", 0777, true ); }
      if (!file_exists($this->images_folder . "watermarks/png/")) { mkdir($this->images_folder . "watermarks/png/", 0777, true ); }
      if (!file_exists($this->images_folder . "thumbs/")) { mkdir($this->images_folder . "thumbs/", 0777, true ); }
    }
    $this->files = $files;
    $this->post = $post;
    $this->title = preg_replace('![^a-z0-9]!imsx', '', $this->post['title']);
    $this->name = preg_replace('![^a-z0-9.]!imsx', '', $this->files['plik']['name']);
    $this->watermark = preg_replace('![^a-z0-9]!imsx', '', $this->post['watermark']);
  }

  function isUploadedFileValidImageFile() {
    //tymczasowo wczytuje do bufora plik w celu analizy zawartosci
    $bufor = file_get_contents($this->files['plik']['tmp_name']);
    $this->temp_name = $this->files['plik']['tmp_name'];

    $finfo = new finfo(FILEINFO_MIME_TYPE);
    $this->mime_type = $finfo->buffer($bufor);
    
    return in_array($this->mime_type, array("image/png", "image/jpg", "image/jpeg"));
  }
  
  function isValidImageFile() {
    
    $extension = strtolower(strrchr($this->files['plik']['name'], '.'));
    $extension = ltrim($extension, '.');
    
    //return in_array($this->files['plik']['type'], array("image/png", "image/jpg", "image/jpeg"));
    return in_array($extension, array("png", "jpg", "jpeg"));
  }
  
  function saveInFolder() {
    //save_in_folder();
    $this->uniq_image_name = uniqid();
    move_uploaded_file($this->temp_name, $this->images_folder . $this->uniq_image_name);
    if ($this->mime_type == "image/jpg" || $this->mime_type == "image/jpeg") {
      $this->image = imagecreatefromjpeg($this->images_folder . $this->uniq_image_name);
    }
    elseif ($this->mime_type == "image/png") {
      $this->image = imagecreatefrompng($this->images_folder . $this->uniq_image_name);
    }
  }
  
  function createThumb() {
    $thumb_size_x = 200;
    $thumb_size_y = 100;
    $this->image_width = imagesx($this->image);
    $this->image_height = imagesy($this->image);
    $image_thumb = imagecreatetruecolor($thumb_size_x, $thumb_size_y);
    
    imagecopyresampled($image_thumb, $this->image, 0, 0, 0, 0, $thumb_size_x, $thumb_size_y, $this->image_width, $this->image_height);
    imagejpeg($image_thumb, $this->images_folder . "/thumbs/" . $this->uniq_image_name, 80);
    imagedestroy($image_thumb);
  }
  
  function createWatermark($watermark_text) {
    $font_src = "./public/ANUDRG__.ttf";
    $font_size = 24;
    
    imagealphablending($this->image, true);
    //alpha channel : 0 - totally not transparent, 127 - totally transparent
    $transparency = 67;
    $font_color = imagecolorallocatealpha($this->image, rand(0,255), rand(0,255), rand(0,255), $transparency);
   
    imagettftext($this->image, $font_size, 0, $this->image_width*0.95-strlen($watermark_text)*$font_size, $this->image_height*0.95, $font_color, $font_src, $watermark_text);
    
    //added for testing content negotiation
    imagepng($this->image, $this->images_folder . "/watermarks/png/" . $this->uniq_image_name);
    imagejpeg($this->image, $this->images_folder . "/watermarks/" . $this->uniq_image_name, 80);
  }
  
  function execute() {
    $return_array = array();
    if ($this->files['plik']['error'] === UPLOAD_ERR_INI_SIZE) { $return_array[] = FILE_TOO_LARGE; } 
    if (!$this->isValidImageFile()) { $return_array[] = NOT_AN_IMAGE; echo "why";} 
    else {
      if ($this->files['plik']['error'] === UPLOAD_ERR_OK) {
        if ($this->isUploadedFileValidImageFile()) {
          $this->saveInFolder();
          $this->createThumb();
          $this->createWatermark($this->watermark);
          
          $xml = new TXMLGenerator();
          
          if (isset($this->post['private'])) {
            //echo $this->post['private'];
            if ($this->post['private'] == 'on') { 
              $xml->addImage(0, $this->uniq_image_name, $_SESSION['uniq_id'], $this->title, $this->name, $this->watermark); 
            }
            else { 
              $xml->addImage(1, $this->uniq_image_name, 0, $this->title, $this->name, $this->watermark); 
            } 
          }
          else { 
            $xml->addImage(1, $this->uniq_image_name, 0, $this->title, $this->name, $this->watermark); 
          } 
          
          
          $return_array[] = OPERATION_SUCCESS;
        }
        else { $return_array[] = NOT_AN_IMAGE; }
      }
    }
    //else { $return_array[] = IMG_UPLOAD_ERROR; }
    return $return_array;
  }
  
}
