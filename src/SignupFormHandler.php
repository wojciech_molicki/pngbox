<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of SignupFormHandler
 *
 * @author Wojtek
 */
require_once('./src/Database.php');
require_once('./src/ErrorCodes.php');

class TSignupFormHandler {
  //put your code here
  function __construct(&$post) {
    
    $this->auth = new TAuthentication();
    
    
    $this->username = $post['username'];
    $this->password = $post['password'];
    $this->password_repeat = $post['password_repeat'];
    
    
    
    //$auth->createUser("Mosinski", "baaaaaaaddddddssss");
    //$auth->createUser("Mosinski", "baaaaaaaddddddssss");
    //$res = $db->queryDatabase("select * from users;");
   // foreach ($res as $r) //{print_r($r);}//
    //{ echo $r['username'] . " " . $r['password'] . "<br />"; }
   
  }
  
  function doPasswordsMatch($password, $password_repeat) {
    return $password == $password_repeat;
  }
  
  function checkFormData($username, $password, $password_repeat) {
    if ($this->auth->checkIfValidPostData($username) + $this->auth->checkIfValidPostData($password) + $this->auth->checkIfValidPostData($password_repeat) != 0) {
      return DATA_NOT_VALID;
    } 
    if ($this->doPasswordsMatch($password, $password_repeat)) {
      return OPERATION_SUCCESS;
    }
    else {
      return PASS_NOT_MATCH;
    }
  }
  
  function execute() {
    $result = $this->checkFormData($this->username, $this->password, $this->password_repeat);
    if ($result == OPERATION_SUCCESS) {
      $result = $this->auth->createUser($this->username, $this->password);
      return $result;
    }
    else { return $result; } 
  }
}
