<?php

/* 
 * Projekt WAI cz2
 * Galeria obrazków
 */

define('GREEN_BOX', 44);
define('RED_BOX', 45);
define('BLUE_BOX', 46);

define('PASS_NOT_MATCH', 47);
define('USER_EXISTS', 48);
define('DATABASE_ERR', 49);
define('OPERATION_SUCCESS', 0);
define('DATA_NOT_VALID', 50);
define('FILE_TOO_LARGE', 51);
define('NOT_AN_IMAGE', 52);
define('IMG_UPLOAD_ERROR', 53);

define ('USER_NOT_IN_DATABASE', 801);
define ('LOGIN_SUCCESS', 800);
define ('WRONG_PASSWORD', 802);
define ('FAIL_LOGIN_EXCEEDED', 803);

define ('JPG', 333);
define ('PNG', 332);

define ('CHECKBOX_SAVE', 666);
define ('CHECKBOX_REMOVE', 667);






