<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of DisplayPage
 *
 * @author Wojtek
 */
require_once('./src/ErrorCodes.php');

class TDisplayPage {
  //put your code here
  function __construct() {
    $this->index = file_get_contents("./public/html_templates/index.html");
    $this->main = '';
    $this->login = file_get_contents("./public/html_templates/login.html");
    $this->signup = file_get_contents("./public/html_templates/signup.html");
    $this->signed_in = file_get_contents("./public/html_templates/signed_in.html");
    $this->nav = file_get_contents("./public/html_templates/nav_bar.html");
    $this->aside = file_get_contents("./public/html_templates/aside.html");
    
  }
  function buildContent() {
    $this->content = $this->index;
    $this->mainContent($this->main);
    $this->navBar($this->nav);
    $this->asideBox($this->aside);
    
  }
  function showContent() {
    $this->buildContent();
    echo $this->content;
  }
  
  //functions for placing various things in placeholders ********************
  function mainContent($content) {
    $this->content = str_replace("{main_content}", $content, $this->content);
  }
  
  function setMainContent($content) {
    $this->main = $content;
  }
  
  function appendToMainContent($content) {
    $this->main = $this->main . $content;
  }
  
  function navBar($content) {
    $this->content = str_replace("{nav_content}", $content, $this->content);
  }
  
  function asideBox($content) {
    $this->content = str_replace("{aside_box}", $content, $this->content);
  }
  
  function middleAside($content) {
    $this->content = str_replace("{middleAside_content}", $content, $this->content);
  }
  
  function bottomAside($content) {
    
    $this->content = str_replace("{bottomAside_content}", $content, $this->content);
  }
  // ************************************************************************
  
  function createBox($message, $mode) {
    if ($mode == RED_BOX) { $box = file_get_contents("./public/html_templates/red_box.html"); }
    elseif ($mode == GREEN_BOX) { $box = file_get_contents("./public/html_templates/green_box.html"); }
    elseif ($mode == BLUE_BOX) { $box = file_get_contents("./public/html_templates/info_box.html"); }
    
    $box = str_replace("{message}", $message, $box);
    return $box;
  }
  
  function showImage($image) {
    $image_watermark_folder = "./public/images/watermarks/";
    $this->index = "";
    
    $png_filename = $image_watermark_folder . "png/" . $image;
    $jpg_filename = $image_watermark_folder . $image;
    
    if (file_exists($png_filename) && $this->browserImagePrefference() == PNG) { $this->content .= "<img src=\"." . $png_filename . "\" />"; }
    else { $this->content .= "<img src=\"." . $jpg_filename . "\" />"; }
    
    echo $this->content;
  }
  
  function showImageDiv($images) {
    $this->index = "";
    $this->content = "";
    $image_thumbs_folder = "./public/images/thumbs/";
    
    foreach ($images as  $uniq_id => $title) {
      $this->content = $this->content . "<img src=\"." . $image_thumbs_folder . $uniq_id . "\" />";
    }
    echo $this->content;
  }
  
  function addToMainContent($html_to_add) {
    $this->main = $this->main . $html_to_add;
  }
  
  function uploadForm() {
    $temp = file_get_contents("./public/html_templates/pic_upload.html");
    if ($_SESSION['logged_in'] == 1) { $temp = str_replace("{private}", file_get_contents("./public/html_templates/private_upload.html"), $temp); }
    else { $temp = str_replace("{private}", '', $temp); }
    $this->addToMainContent($temp);
  }
  
  function signedIn() {
    $this->signed_in = str_replace("{username}", $_SESSION['username'], $this->signed_in);
    $this->aside = str_replace("{aside_content}", $this->signed_in, $this->aside);
  }
  
  function logIn() {
    $this->aside = str_replace("{aside_content}", $this->login, $this->aside);
  }
  
  function signUp() {
    $this->aside = str_replace("{aside_content}", $this->signup, $this->aside);
  }
  
  function rebuildAside() {
    $this->aside = file_get_contents("./public/html_templates/aside.html");
  }
  
  function browserImagePrefference() {
    //returns PNG for png, JPG for jpg prefference
    $header = $_SERVER['HTTP_ACCEPT'];
     
    $jpg = strpos($header, "image/jpeg");
    $png = strpos($header, "image/png");
    $q_jpg = 0;
    $q_png = 0;
    
    //
    if ($jpg) { $q_jpg = substr($header, strpos($header, "q=",  $jpg)+2, 3). "<br />"; }
    if ($png) { $q_png = substr($header, strpos( $header, "q=", $png)+2, 3). "<br />"; }
    
    if ($q_jpg >= $q_png) { return JPG; } else { return PNG; }
  }
}
