<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**s
 * Description of ParseURI
 *
 * @author Wojtek
 */
require_once('./src/DisplayPage.php');
require_once('./src/UploadFileHandler.php');
require_once('./src/SignupFormHandler.php');
require_once('./src/Gallery.php');
require_once('./src/Authentication.php');
require_once('./src/ErrorCodes.php');
require_once('./src/XMLGenerator.php');

class TParseURI {
  function __construct($uri, &$post, &$files) {
    $page = new TDisplayPage();
    $db = new TDatabase();
    
    $this->query = '';
    
    if (strlen($uri) > 64) {
      die("ERROR: too long uri");
    }
    $uri = str_replace('/galeria/index.php', '', $uri);
    
    if(isset($_GET['q'])) { 
     
      $this->query = preg_replace('![^a-z0-9.]!imsx','', $_GET['q']);
      $uri = str_replace('q=' . $_GET['q'], '', $uri);
    }
    $uri = preg_replace('![^a-z0-9]!imsx','', $uri);
    
    $this->generateSidePanel($uri, $page);
    
    $this->handleURI($uri, $post, $page, $files, $post);
    
    $page->showContent();
  }
  
  function generateSidePanel(&$uri, &$page) {
    if ($_SESSION['logged_in'] == 1) {
      $page->signedIn();
    }
    elseif ($uri == 'signup') {
      $page->signUp();
    }
    else {
      $page->logIn();
    }
  }
  
  function handleView(&$page) {
    if ($this->query != '') {
      $xml = new TXMLGenerator();
      $public_images = $xml->getPublicImages();
      //if in public, display
      if (in_array(explode('.',$this->query)[0], $public_images)) { $page->showImage($this->query); }
      else {
        if ($_SESSION['logged_in'] == 0) { 
          if (in_array(explode('.',$this->query)[0], $public_images)) { $page->showImage($this->query); }
          else { $c = $page->createBox("You do not have permission to view this resource", RED_BOX); $page->addToMainContent($c); }
        }
        elseif ($_SESSION['logged_in'] == 1) {
          $user_images = $xml->getUserPrivateImagesNames($_SESSION['uniq_id']);
          if (in_array(explode('.',$this->query)[0], $user_images)) { $page->showImage($this->query); }
          else { $c = $page->createBox("You do not have permission to view this resource", RED_BOX); $page->addToMainContent($c); }
        }
      }
    }
  }
  
  function handleUpload(&$page, &$files, &$post) {
    if ($_SESSION['logged_in'] == 0) {
        $c = $page->createBox("Notice - If you are not logged in, image will be added to a public-access gallery.", BLUE_BOX);
        $page->addToMainContent($c);
    }
    elseif ($this->query != '') {
      $result_array = array();
      $q = preg_replace('![^a-z0-9.]!imsx','', ($_REQUEST['q']));
      $q = strtolower($q);
      $xml = new TXMLGenerator();
      $images = $xml->getUserImagesTitlesAndNames($_SESSION['uniq_id']);
      $images = array_merge($images, $xml->getPublicImagesTitlesAndNames());
      foreach ($images as $uniq_id => $title) {
        if (stristr($q, substr($title, 0, strlen($q)))) {
          $result_array[$uniq_id] = $title; 
        }
      }
      if ($this->query != '') { $page->showImageDiv($result_array); }
    }
        
    if (!empty($post)) {
      $handler = new TUploadFileHandler($post, $files);
      $arr = $handler->execute();

      foreach ($arr as $result) {
        switch ($result)
        {
          case OPERATION_SUCCESS:
            $c = $page->createBox("Upload succesful!", GREEN_BOX);
            break;
          case FILE_TOO_LARGE:
            $c = $page->createBox("File too large.", RED_BOX);
            break;
          case NOT_AN_IMAGE:
            $c = $page->createBox("File is not an image.", RED_BOX);
            break;
          default:
            $c = $page->createBox("Unrecognised error.", RED_BOX);
            break;
        }
        $page->appendToMainContent($c);
      }
    } 
    $page->uploadForm();
  }
  
  function handleLogin(&$post, &$page) {
    if (!empty($post)) {
      $username = $post['username'];
      $password = $post['password'];

      $auth = new TAuthentication();
      $result = $auth->checkUserPass($username, $password);
      switch ($result) {
        case LOGIN_SUCCESS:
          $c = $page->createBox("You just logged in!", GREEN_BOX);
          $page->rebuildAside();
          $page->signedIn();
          //header("Location: /galeria/index.php/login");
          break;
        case FAIL_LOGIN_EXCEEDED:
          $c = $page->createBox("Too many fail logins! Contact web admin to unlock your account.", RED_BOX);
          break;
        case WRONG_PASSWORD:
          $c = $page->createBox("Wrong password!", RED_BOX);
          break;
        case USER_NOT_IN_DATABASE:
          $c = $page->createBox("There is no such user.", BLUE_BOX);
          break;
        case DATA_NOT_VALID:
          $c = $page->createBox("You must use alphanumeric form data.", BLUE_BOX);
          break;
        default:
          $c = $page->createBox("Unknown error!", RED_BOX);
          break;
      }
      $page->setMainContent($c);
    }
  }
  
  function handleSignup(&$post, &$page) {
    if (!empty($post)) {
      $handler = new TSignupFormHandler($post);
      $result = $handler->execute();
      switch ($result) {
        case OPERATION_SUCCESS:
          $c = $page->createBox("You just signed up!", GREEN_BOX);
          $page->rebuildAside();
          $page->logIn();
          break;
        case USER_EXISTS:
          $c = $page->createBox("User already exists!", RED_BOX);
          break;
        case DATA_NOT_VALID:
          $c = $page->createBox("You must use alphanumeric form data.", BLUE_BOX);
          break;
        case PASS_NOT_MATCH:
          $c = $page->createBox("Passwords must match.", BLUE_BOX);
          break;
        default:
          $c = $page->createBox("Unknown error!", RED_BOX);
          break;
      }
      $page->setMainContent($c);
    }
  }
  
  function handleSettings()
  {
    
  }
  
  function removeMarked(&$post) {
    if (isset($post['to_be_removed'])) {
      foreach ($post['to_be_removed'] as $pic) {
        $key = array_search($pic, $_SESSION['saved_pic_array']);
        unset($_SESSION['saved_pic_array'][$key]);
      }
    }
    header("Location: ./browsesaved");
  }
  
  function handleURI($uri, &$post, &$page, &$files, &$post) {
    if ($uri == '/' || $uri == '') {
      
      $c = $page->createBox("test!", BLUE_BOX);
      $page->setMainContent($c);
      $page->appendToMainContent("<script>document.getElementById('home').style.backgroundColor = \"#88aa00\";</script>");
    }
    if ($uri == 'signup') {
      $this->handleSignup($post, $page);
    }
    
    if ($uri == 'login') {
      $this->handleLogin($post, $page);
    }
    
    if ($uri == 'mypictures') {
      $_SESSION['saved_pic_array'] = $post['to_be_saved'];
      header("Location: ./browse");
    }
    
    if ($uri == 'remove') {
      $this->removeMarked($post);
    }
    
    if ($uri == 'browsesaved') {
      
      $gallery = new TGallery();
      $page->setMainContent($gallery->buildGalleryFromSaved($_SESSION['saved_pic_array']));
      $page->appendToMainContent("<script>document.getElementById('saved').style.backgroundColor = \"#88aa00\";</script>");
    }
    if ($uri == 'signout') {
     $_SESSION['logged_in'] = 0;
     session_destroy();
     header("Location: /galeria/index.php");
    }
    
    if ($uri == 'upload') {
      $page->appendToMainContent("<script>document.getElementById('upload').style.backgroundColor = \"#88aa00\";</script>");
      $this->handleUpload($page, $files, $post);
    }

    if ($uri == 'browse') {
      
      $gallery = new TGallery();
      $page->setMainContent($gallery->buildGallery());
      $page->appendToMainContent("<script>document.getElementById('browse').style.backgroundColor = \"#88aa00\";</script>");
    }
    
    if ($uri == 'view') {
      $this->handleView($page);
    }
    
    if ($uri == 'settings') {
      $this->handleSettings($page);
    }
  }
}


