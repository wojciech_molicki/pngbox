<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of Gallery
 *
 * @author Wojtek
 */
//require_once("./Authentication.php");
require_once('./src/XMLGenerator.php');

class TGallery {
  function __construct() {
    $this->image_folder = "./public/images/thumbs/";
    $this->content = "";
  }
  
  function buildGallery() {
    //returns html code with pictures
    $this->content = "<form action=\"./mypictures\" method=\"post\">";
    $this->content = $this->content . "<input type=\"submit\" name=\"save\" value=\"Save checked\"/><hr />";
    
    $xml = new TXMLGenerator();
    $public_images = $xml->getPublicImages();
    $private_images_html = '';
    //xml->rebuild();
    foreach ($public_images as $entry) {
      $this->content = $this->content . $this->makeImgDivBox($entry, CHECKBOX_SAVE);
    }
    
    if ($_SESSION['logged_in']) {
      $user_privates = $xml->getUserPrivateImagesNames( $_SESSION['uniq_id']);
      foreach ($user_privates as $private_image) { $private_images_html .= $this->makeImgDivBox($private_image, CHECKBOX_SAVE); }
      $this->content = $this->content . "<br /><hr id=\"gallery_spacer\" />Private images:<hr />";
      $this->content = $this->content . $private_images_html; 
    }
    
    $this->content = $this->content . "</form>";
    return $this->content;
  }
  
  function buildGalleryFromSaved($array) {
    if (!empty($_SESSION['saved_pic_array'])) {
      $this->content = "<form action=\"./remove\" method=\"post\">";
      $this->content = $this->content . "<input type=\"submit\" name=\"remove\" value=\"Remove checked\"/><hr />";
      foreach ($array as $entry) {
        if ($entry != "." && $entry != "..") {
           $this->content = $this->content . $this->makeImgDivBox($entry, CHECKBOX_REMOVE);
        }
      }
      $this->content = $this->content . "</form>";
    return $this->content;
    }
  }
  
  function makeImgDivBox($entry, $save) {
    //returns html code for div box containing img and checkbox
    // $save determines whether images will be saved to or removed from session array
    $checked = '';
    if (!empty($_SESSION['saved_pic_array'])) { if (in_array($entry, $_SESSION['saved_pic_array'])) { $checked = 'checked'; } }
    
    $box = "<div class=\"imgBox\">";
    $box = $box . "<a target=\"_blank\" href=\"" . "../index.php/view?q=" . $entry . "\" alt=\"\"><img src=\".". $this->image_folder . $entry .  "\" /></a>";
    
    if ($save == CHECKBOX_SAVE) { $box = $box . "<input type=\"checkbox\" name=\"to_be_saved[]\" value=\"$entry\" $checked />"; }
    elseif ($save == CHECKBOX_REMOVE) { $box = $box . "<input type=\"checkbox\" name=\"to_be_removed[]\" value=\"$entry\" />"; }
    $box = $box . "</div>";
    return $box;
  }
}
