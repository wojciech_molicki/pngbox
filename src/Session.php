<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of Session
 *
 * @author Wojtek
 */
class TSession {
  function __construct() {
    //creates a session
    session_start();
    
    // safeguarding against session fixation with adding ?PHPSESSID=xyz
    // to the URI
    if (!isset($_SESSION['initiated'])) {
      session_regenerate_id();
      $_SESSION['initiated'] = true;
      $_SESSION['remote_ip'] = $_SERVER['REMOTE_ADDR'];
      $_SESSION['logged_in'] = 0;
      $_SESSION['saved_pic_array'] = array();
    }
    
    // safeguarding against session steal by copying URI adress
    // with cookies disabled and sending it to attacker
    if ($_SESSION['remote_ip'] != $_SERVER['REMOTE_ADDR']) {
      $delete_old_session = true;
      session_regenerate_id($delete_old_session);
    }
    
    
  }
  
  function logout() {
    // deletes cookie and session id 
    // session_
    session_destroy();
  }
}
