<?php

/*
 * Projekt WAI cz2
 * Galeria obrazków
 */

/**
 * Description of XMLGenerator
 *
 * @author Wojtek
 */
class TXMLGenerator {
  //put your code here
  function __construct() {
    $this->ximagefile = './db/images.xml';
    if (!file_exists($this->ximagefile)) { $this->xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><Images></Images>"); }
    else { $this->xml = simplexml_load_file($this->ximagefile); }
  }
  function addImage($public, $uniq_id, $user_id, $title, $filename, $watermark) {
    
    $img = $this->xml->addChild("img");
    if ($public == 0) { $img->addAttribute("public", "no"); }
    else { $img->addAttribute("public", "yes"); }
    $img->addChild("uniq_id", $uniq_id);
    if ($public == 0) { $img->addChild("user_id", $user_id); }
    $img->addChild("title", $title);
    $img->addChild("watermark", $watermark);
    $img->addChild("filename", $filename);
    
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($this->xml->asXML());
    $dom->save($this->ximagefile);
    
  }
  
  function rebuild()
  {
    //rebuilds xml files basing on existing images
    //intended as administrative option
    $h = opendir("./public/images/thumbs/");
    $files = array();
    while($file = readdir()) { $files[] = $file; }
    $h.closedir();
            
    $doc = new DOMDocument('1.0'); 
    $doc->load($this->ximagefile);
    $thedocument = $doc->documentElement;
    $list = $thedocument->getElementsByTagName('img');
    
    foreach($list as $element) {
      $res = $element->getElementsByTagName('uniq_id');
      
      if (!in_array((string)$res->item(0)->nodeValue, $files)) { $thedocument->removeChild($element); }
    }
    $doc->save($this->ximagefile);
    
  }
  
  function searchPrivateImages($userid) {
    //returns simplexmlobject array of matching private images
    $resultArray = array();
    $matches = $this->xml->xpath('img[@public=\'no\']');
    
    foreach ($matches as $match) {
      if ($match->user_id == $userid) { $resultArray[] = $match; }
    }
    return $resultArray;
  }
  
  function searchImages($userid) {
    //returns simplexmlobject array of matchingimages
    $resultArray = array();
    $matches = $this->xml->xpath('img');
    
    foreach ($matches as $match) {
      if ($match->user_id == $userid) { $resultArray[] = $match; }
    }
    return $resultArray;
  }
  
  function getPublicImages() {
    //returns array of matching public images
    $resultArray = array();
    $matches = $this->xml->xpath('img[@public=\'yes\']');
    
    foreach ($matches as $match) {
     $resultArray[] = (string)$match->uniq_id; 
    }
    return $resultArray;
  }
  
  function getPublicImagesTitlesAndNames() {
    //returns array of matching public images
    $resultArray = array();
    $matches = $this->xml->xpath('img[@public=\'yes\']');
    
    foreach ($matches as $match) {
     $resultArray[(string)$match->uniq_id] = (string)$match->title; 
    }
    return $resultArray;
  }
  
  function getUserPrivateImagesNames($userid) {
    $resultArray = array();
    $arr = $this->searchPrivateImages($userid);
    foreach ($arr as $match) {
      $resultArray[] = (string)$match->uniq_id;
    }
    return $resultArray;
  }
  
  function getUserPrivateImagesTitles($userid) {
    $resultArray = array();
    $arr = $this->searchPrivateImages($userid);
    foreach ($arr as $match) {
      $resultArray[] = (string)$match->title;
    }
    return $resultArray;
  }
  
  function getUserImagesTitlesAndNames($userid) {
    //returns assiotiative array with images (uniq_id => title)
    $resultArray = array();
    $arr = $this->searchImages($userid);
    foreach ($arr as $match) {
      $resultArray[(string)$match->uniq_id] = (string)$match->title;
    }
    return $resultArray;
  }
  
  function getAllPrivateImagesNames() {
    $resultArray = array();
    $matches = $this->xml->xpath('img[@public=\'no\']');
    foreach ($matches as $match) {
      $resultArray[] = (string)$match->uniq_id;
    }
    return $resultArray;
  }
}

